# Wwise Granulator

A granular delay Wwise plugin programmed in C++ based on my implementation in JUCE. Currently in progress.

* The Granulator project builds the Wwise Authoring part of the plug-in.
* The GranulatorFX project builds the Sound Engine part of the plug-in.