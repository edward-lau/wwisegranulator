//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Granulator.rc
//
#define IDD_GRANULATOR_BIG              101
#define IDC_STATIC_INPUTGAIN            1006
#define IDC_STATIC_BLEND                1007
#define IDC_STATIC_RANDOMPAN            1008
#define IDC_STATIC_RANDOMLEVEL          1009
#define IDC_STATIC_RANDOMDELAY          1010
#define IDC_STATIC_DELAY                1011
#define IDC_STATIC_PITCH                1012
#define IDC_STATIC_RANDOMPITCH          1013
#define IDC_STATIC_DURATION             1014
#define IDC_STATIC_RANDOMDURATION       1015
#define IDC_RANGE_INPUTGAIN             1016
#define IDC_RANGE_BLEND                 1017
#define IDC_RANGE_RANDOMLEVEL           1018
#define IDC_RANGE_RANDOMPAN             1019
#define IDC_RANGE_DELAY                 1020
#define IDC_RANGE_RANDOMDELAY           1021
#define IDC_RANGE_PITCH                 1022
#define IDC_RANGE_RANDOMPITCH           1023
#define IDC_RANGE_DURATION              1024
#define IDC_RANGE_RANDOMDURATION        1025
#define IDC_RANGE_DENSITY               1026
#define IDC_CHECK_PROCESSLFE            1028

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1029
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
