#include "stdafx.h"
#include "resource.h"
#include "GranulatorPlugin.h"
#include <GranulatorFXFactory.h>
#include <AK/Tools/Common/AkAssert.h>

using namespace AK;
using namespace Wwise;

// Granulator parameter names
static LPCWSTR szInputGain = L"InputGain";
static LPCWSTR szBlend = L"Blend";
static LPCWSTR szRandomLevel = L"RandomLevel";
static LPCWSTR szRandomPan = L"RandomPan";
static LPCWSTR szDelay = L"Delay";
static LPCWSTR szRandomDelay = L"RandomDelay";
static LPCWSTR szPitch = L"Pitch";
static LPCWSTR szRandomPitch = L"RandomPitch";
static LPCWSTR szDuration = L"Duration";
static LPCWSTR szRandomDuration = L"RandomDuration";
static LPCWSTR szDensity = L"Density";
static LPCWSTR szProcessLFE = L"ProcessLFE";

// If necessary, bind non static text UI controls to plug-in properties here
AK_BEGIN_POPULATE_TABLE(GranularProp)
    AK_POP_ITEM(IDC_CHECK_PROCESSLFE, szProcessLFE)
AK_END_POPULATE_TABLE()

GranulatorPlugin::GranulatorPlugin()
: m_pPSet(NULL),
  m_hwndPropView(NULL)
{
}

GranulatorPlugin::~GranulatorPlugin()
{
}

/*
Called when user deletes an instance of the plug-in.
Release memory and delete the object itself.
*/
void GranulatorPlugin::Destroy()
{
    delete this;
}

/*
Called when an instance of the plug-in is created.
*/
void GranulatorPlugin::SetPluginPropertySet(AK::Wwise::IPluginPropertySet* in_pPSet)
{
    m_pPSet = in_pPSet;
}

/*
-Lets Wwise know where to look for Dialog resources.
*/
HINSTANCE GranulatorPlugin::GetResourceHandle() const
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    return AfxGetStaticModuleState()->m_hCurrentResourceHandle;
}

/*
-Called by Wwise to get plug-in dialog propertiesS.
-Effect plug-ins only have one dialog (Effect Settings tab in Effects Editor).
-If not providing dialog resources, Wwise will create one dynamically based on XML content.
*/
bool GranulatorPlugin::GetDialog(eDialog in_eDialog, 
                                 UINT& out_uiDialogID, 
                                 AK::Wwise::PopulateTableItem*& out_pTable) const
{
    AKASSERT(in_eDialog == SettingsDialog);

    out_uiDialogID = IDD_GRANULATOR_BIG;
    out_pTable = GranularProp;

    return true;
}

/*
-Stores current parameters as a data block into the SoundBank.
-The block will be loaded from the SoundBank into the sound engine plug-in parameters.
*/
bool GranulatorPlugin::GetBankParameters(const GUID& in_guidPlatform,
                                         AK::Wwise::IWriteData* in_pDataWriter) const
{
    CComVariant varProp;

    // Data must be written in the same order declared in the sound engine parameter structure.
    
    // Input Gain
    m_pPSet->GetValue(in_guidPlatform, szInputGain, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Blend
    m_pPSet->GetValue(in_guidPlatform, szBlend, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Random Level
    m_pPSet->GetValue(in_guidPlatform, szRandomLevel, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Random Pan
    m_pPSet->GetValue(in_guidPlatform, szRandomPan, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Delay
    m_pPSet->GetValue(in_guidPlatform, szDelay, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Random delay
    m_pPSet->GetValue(in_guidPlatform, szRandomDelay, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Pitch
    m_pPSet->GetValue(in_guidPlatform, szPitch, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Random Pitch
    m_pPSet->GetValue(in_guidPlatform, szRandomPitch, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Duration
    m_pPSet->GetValue(in_guidPlatform, szDuration, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Random Duration
    m_pPSet->GetValue(in_guidPlatform, szRandomDuration, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Density
    m_pPSet->GetValue(in_guidPlatform, szDensity, varProp);
    in_pDataWriter->WriteReal32(varProp.fltVal);
    // Process LFE
    m_pPSet->GetValue(in_guidPlatform, szProcessLFE, varProp);
    in_pDataWriter->WriteBool(varProp.boolVal != 0);

    return true;
}