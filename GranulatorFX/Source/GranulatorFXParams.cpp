#include <AK/Tools/Common/AkAssert.h>
#include <AK/Tools/Common/AkBankReadHelpers.h>
#include <math.h>
#include "GranulatorFXParams.h"

// Constructor
GranulatorFXParams::GranulatorFXParams()
{
}

// Copy constructor
GranulatorFXParams::GranulatorFXParams(const GranulatorFXParams& copy)
{
    RTPC = copy.RTPC;
    RTPC.hasChanged = true;
    NonRTPC = copy.NonRTPC;
    NonRTPC.hasChanged = true;
}

// Destructor
GranulatorFXParams::~GranulatorFXParams()
{
}

/*
-Duplicates parameter instance and adjust necessary state variables 
to be used with a new plug-in instance (ie. event creates new playback instance).
*/
AK::IAkPluginParam* GranulatorFXParams::Clone(AK::IAkPluginMemAlloc* in_pAllocator)
{
    return AK_PLUGIN_NEW(in_pAllocator, GranulatorFXParams(*this));
}

/*
-Initialize parameters with provided parameter block.
-When block size is 0 (ie. plugin is used within authoring tool), initialize with default values.
*/
AKRESULT GranulatorFXParams::Init(AK::IAkPluginMemAlloc* in_pAllocator,
                                  const void*            in_pParamsBlock,
                                  AkUInt32               in_uBlockSize)
{
    if (in_uBlockSize == 0)
    {
        // Initialize default parameters
//         NonRTPC.maxDelayTime = MAXDELAYTIME;

        RTPC.inputGain = PARAM_DEFAULT_INPUTGAIN;
        RTPC.blend = PARAM_DEFAULT_BLEND;
        RTPC.randomLevel = PARAM_DEFAULT_RANDOMLEVEL;
        RTPC.randomPan = PARAM_DEFAULT_RANDOMPAN;
        RTPC.delay = PARAM_DEFAULT_DELAY;
        RTPC.randomDelay = PARAM_DEFAULT_RANDOMDELAY;
        RTPC.pitch = PARAM_DEFAULT_PITCH;
        RTPC.randomPitch = PARAM_DEFAULT_RANDOMPITCH;
        RTPC.duration = PARAM_DEFAULT_DURATION;
        RTPC.randomDuration = PARAM_DEFAULT_RANDOMDURATION;
        RTPC.density = PARAM_DEFAULT_DENSITY;
        RTPC.hasChanged = true;

        NonRTPC.processLFE = PARAM_DEFAULT_PROCESSLFE;
        NonRTPC.hasChanged = true;

        return AK_Success;
    }

    return SetParamsBlock(in_pParamsBlock, in_uBlockSize);
}

/*
-Called by the sound engine when parameter node is terminated. 
-Release memory here and delete parameter node.
*/
AKRESULT GranulatorFXParams::Term(AK::IAkPluginMemAlloc* in_pAllocator)
{
    AK_PLUGIN_DELETE(in_pAllocator, this);
    return AK_Success;
}

/*
-Sets all parameters at once using a parameter block stored during SoundBank creation.
*/
AKRESULT GranulatorFXParams::SetParamsBlock(const void* in_pParamsBlock,
                                            AkUInt32    in_uBlockSize)
{
    AKRESULT eResult = AK_Success;
    AkUInt8* pParamsBlock = (AkUInt8*)in_pParamsBlock;
    
//     NonRTPC.maxDelayTime = MAXDELAYTIME;

    RTPC.inputGain = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.blend = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.randomLevel = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.randomPan = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.delay = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.randomDelay = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.pitch = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.randomPitch = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.duration = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.randomDuration = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    RTPC.density = READBANKDATA(AkReal32, pParamsBlock, in_uBlockSize);
    NonRTPC.processLFE = READBANKDATA(bool, pParamsBlock, in_uBlockSize);
    
    CHECKBANKDATASIZE(in_uBlockSize, eResult);
    
    NonRTPC.hasChanged = true;
    RTPC.hasChanged = true;

    return eResult;
}

/*
-Called whenever a parameter value changes via UI, RTPCs etc...
*/
AKRESULT GranulatorFXParams::SetParam(AkPluginParamID in_paramID, 
                                      const void*     in_pValue, 
                                      AkUInt32        in_uParamSize)
{
    AKRESULT eResult = AK_Success;

    switch (in_paramID)
    {
    case AK_PARAM_ID_INPUTGAIN:
        RTPC.inputGain = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_BLEND:
        RTPC.blend = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_RANDOMLEVEL:
        RTPC.randomLevel = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_RANDOMPAN:
        RTPC.randomPan = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_DELAY:
        RTPC.delay = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_RANDOMDELAY:
        RTPC.randomDelay = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_PITCH:
        RTPC.pitch = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_RANDOMPITCH:
        RTPC.randomPitch = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_DURATION:
        RTPC.duration = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_RANDOMDURATION:
        RTPC.randomDuration = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_DENSITY:
        RTPC.density = *(AkReal32*)(in_pValue);
        RTPC.hasChanged = true;
        break;
    case AK_PARAM_ID_PROCESSLFE:
        NonRTPC.processLFE = *(bool*)(in_pValue);
        NonRTPC.hasChanged = true;
        break;
    default:
        AKASSERT(!"Invalid paramter.");
        eResult = AK_InvalidParameter;
    }

    return eResult;
}