#include "GranulatorFXDSP.h"
#include <AK/Tools/Common/AkPlatformFuncs.h>
#include <AK/SoundEngine/Common/AkSimd.h>

#include <math.h>

AkUInt32 GranulatorFXDSP::s_randomSeed = 22222;
AkReal32 GranulatorFXDSP::s_twelfthRootOfTwo = powf(2, 1.0f / 12);

GranulatorFXDSP::GranulatorFXDSP()
: m_pDelayData(NULL),
  m_numProcessedChannels(0),
  m_delayPosition(0)
{
}

GranulatorFXDSP::~GranulatorFXDSP()
{

}

void GranulatorFXDSP::Setup(GranulatorFXParams* pInitialParams, 
                            bool                isSendMode, 
                            AkUInt32            sampleRate)
{
    m_sendMode = isSendMode;
    m_params = *pInitialParams;
    m_sampleRate = sampleRate;

    GenerateEnvelope(m_envelope);

    // Setup grain objects
    for (AkUInt32 i = 0; i < NUMGRAINS; i++)
    {
        m_grains[i].Initialize(MAXDELAYTIME, m_envelope, ENVELOPELENGTH, sampleRate);
    }
}

// Delay line initialization/re-initialization.
AKRESULT GranulatorFXDSP::InitDelay(AK::IAkPluginMemAlloc* pAllocator, 
                                    GranulatorFXParams*    pParams, 
                                    AkChannelConfig        channelConfig)
{
    m_delayMemory.Term(pAllocator);
    m_delayPosition = 0;

    m_numProcessedChannels = channelConfig.uNumChannels;
    m_processLFE = pParams->NonRTPC.processLFE;
    
    if (channelConfig.HasLFE() && m_processLFE)
        --m_numProcessedChannels;
    
    if (m_numProcessedChannels == 0)
        return AK_Fail;

    return m_delayMemory.Init(pAllocator, (AkUInt32)(MAXDELAYTIME * m_sampleRate), 1);
}

// Reset delay line and plugin state
void GranulatorFXDSP::ResetDelay()
{
    // Reset delay line
    m_delayMemory.Reset();
    m_delayPosition = 0;
    m_interonsetSamples = 1024;

    // Reset ramp objects
    const AkReal32 rampTime = 0.05f;
    m_inputGainRamp.Setup(m_sampleRate, rampTime);
    m_blendRamp.Setup(m_sampleRate, rampTime);

    // Deactivate all grains
    for (int i = 0; i < NUMGRAINS; i++)
    {
        m_grains[i].isActive = false;
    }
}

// Terminate delay line
void GranulatorFXDSP::TermDelay(AK::IAkPluginMemAlloc* pAllocator)
{
    m_delayMemory.Term(pAllocator);

    for (int i = 0; i < NUMGRAINS; i++)
    {
        m_grains[i].isActive = false;
    }
}

void GranulatorFXDSP::ComputeTailLength(bool in_bFeedbackEnabled, AkReal32 in_fFeedbackValue)
{
    m_tailLength = m_delayMemory.GetDelayLength();
}

// Process audio buffer
void GranulatorFXDSP::Process(AkAudioBuffer*      io_pBuffer, 
                              GranulatorFXParams* pCurrentParams)
{
    m_AkFXTailHandler.HandleTail(io_pBuffer, 0);
    const AkUInt32 numFrames = io_pBuffer->uValidFrames;
    const AkReal32 summingGain = 1.0f / m_numProcessedChannels;
    m_pDelayData = (AkReal32* AK_RESTRICT) m_delayMemory.GetCurrentPointer(0, 0);

    // Give value ramp objects new target values
    m_inputGainRamp.SetTarget(m_params.RTPC.inputGain);
    m_blendRamp.SetTarget(m_params.RTPC.blend);

    AkUInt32 framesProduced = 0;
    while (framesProduced < numFrames)
    {
        // count down interonset time and activate grain if necessary
        --m_interonsetSamples;
        if (m_interonsetSamples <= 0)
        {
            ActivateGrain();
            const AkReal32 interonsetTime = -logf((PseudoRandomNumber() + 1) * 0.5f) / m_params.RTPC.density;
            m_interonsetSamples = (AkUInt32)(interonsetTime * m_sampleRate);
            AkReal32 test = (PseudoRandomNumber() + 1) * 0.5f;
        }

        // Process active grains into output sample
        AkReal32 grainOutput = 0.0f;
        for (AkUInt32 grain = 0; grain < NUMGRAINS; grain++)
        {
            if (m_grains[grain].isActive)
            {
                m_grains[grain].Process(grainOutput, m_pDelayData);
            }
        }

        // Sum input channel data into delay line and write grain output to each channel
        const AkReal32 inputGain = m_inputGainRamp.GetNextValue();
        const AkReal32 blend = m_blendRamp.GetNextValue();

        AkReal32 sumInput = 0.0f;
        for (AkUInt32 channel = 0; channel < m_numProcessedChannels; channel++)
        {
            AkReal32* AK_RESTRICT pChannelData = (AkReal32* AK_RESTRICT)io_pBuffer->GetChannel(channel);
            
            // Add channel data to input sum
            sumInput += pChannelData[framesProduced];

            // Write grain to channel data
            pChannelData[framesProduced] = (pChannelData[framesProduced] * (1 - blend)) + (grainOutput * blend);
        }
        m_pDelayData[m_delayPosition] = sumInput * summingGain * inputGain;

        // Increment delay position
        m_delayPosition = (m_delayPosition < m_delayMemory.GetDelayLength() ? m_delayPosition + 1 : 0);

        framesProduced++;
    }

    // Zero out the LFE channel in send effect configuration if it is not doubled, 
    // to avoid doubling of in-phase unprocessed LFE channel with the one from the dry path.
    // Note: LFE channel is always the last channel
    AkReal32* pLFEChan = io_pBuffer->GetLFE();
    if (pLFEChan != NULL && m_sendMode && !pCurrentParams->NonRTPC.processLFE)
    {
        AkZeroMemLarge(pLFEChan, numFrames * sizeof(AkReal32));
    }
    
    m_params = *pCurrentParams;
}

void GranulatorFXDSP::ActivateGrain()
{
    for (AkUInt32 grain = 0; grain < NUMGRAINS; grain++)
    {
        if (m_grains[grain].isActive == false)
        {
            GrainParameters grainParams;

            ///////////////////////////////////////////////////////////////////////////
            // Grain duration
            AkReal32 grainDuration = m_params.RTPC.duration + (PseudoRandomNumber() * (m_params.RTPC.randomDuration));
            grainDuration *= 0.001;
            
            if (grainDuration < 0.016) // TODO: get rid of branching statement?
                grainDuration += m_params.RTPC.randomDuration;
            else if (grainDuration > 0.9)
                grainDuration -= m_params.RTPC.randomDuration;

            grainParams.durationSamples = (AkInt32)(grainDuration * m_sampleRate);

            ///////////////////////////////////////////////////////////////////////////
            // Grain pitch ratio (sample increment)
            const AkReal32 grainPitch = m_params.RTPC.pitch + (PseudoRandomNumber() * m_params.RTPC.randomPitch);
            grainParams.pitchRatio = SemitonesToPitchRatio(grainPitch);

            ///////////////////////////////////////////////////////////////////////////
            // Relative grain delay time
            AkReal32 readPosition = m_params.RTPC.delay + (PseudoRandomNumber() * m_params.RTPC.randomDelay);
            
            if (readPosition < 0)
                readPosition += m_params.RTPC.delay;
            else if (readPosition > 1.0)
                readPosition -= m_params.RTPC.delay;

            ///////////////////////////////////////////////////////////////////////////
            // Actual grain delay time (determines read position of grain relative to write position)
            AkReal32 grainDelayTime = readPosition * MAXDELAYTIME;
            const AkReal32 endPositionTime = (grainParams.pitchRatio * grainDuration) - grainDelayTime;

            // If read position will overtake write position, adjust delay time to prevent this.
            if (endPositionTime < 0)
                grainDelayTime += endPositionTime;

            // If write position will overtake read position, adjust delay time to prevent this
            if ((grainDuration - endPositionTime) > MAXDELAYTIME)
                grainDelayTime -= endPositionTime;

            grainParams.delayTime = grainDelayTime;
            grainParams.delayPosition = m_delayPosition;
            
            ///////////////////////////////////////////////////////////////////////////
            // Grain level
            grainParams.level = AK_DBTOLIN(((PseudoRandomNumber() + 1) * 0.5) * m_params.RTPC.randomLevel);

            m_grains[grain].Activate(grainParams);
            break;
        }
    }
}

// TODO: This is temp. Generate attack/decay envelope per grain.
void GranulatorFXDSP::GenerateEnvelope(AkReal32* pEnvelopeTable)
{
    // Hann envelope
    for (AkUInt32 i = 0; i < ENVELOPELENGTH; i++)
    {
        pEnvelopeTable[i] = 0.5f - 0.5f * cos(i * 2.0f * DOUBLE_PI / ENVELOPELENGTH);
    }
}

// Linear congruential method for generating pseudo-random number (float version)
AkReal32 GranulatorFXDSP::PseudoRandomNumber()
{
    // Generate a (pseudo) random number (32 bits range)
    s_randomSeed = (s_randomSeed * 196314165) + 907633515;
    // Generate rectangular PDF
    AkInt32 iRandVal = ((AkInt32)s_randomSeed);
    // Scale to floating point range
    AkReal32 fRandVal = iRandVal * ONEOVERMAXRANDVAL;
    AKASSERT(fRandVal >= -1.f && fRandVal <= 1.f);
    // Output
    return fRandVal;
}

AkReal32 GranulatorFXDSP::SemitonesToPitchRatio(AkReal32 semitones)
{
    const AkReal32 pitchRatio = powf(s_twelfthRootOfTwo, semitones);
    return pitchRatio;
}