#ifndef GRANULATORFXDSP_H_
#define GRANULATORFXDSP_H_

#include <AK/SoundEngine/Common/AkTypes.h>
#include <AK/Plugin/PluginServices/AkFXTailHandler.h>
#include <AK/SoundEngine/Common/IAkPluginMemAlloc.h>
#include <AK/SoundEngine/Common/AkCommonDefs.h>
#include "GranulatorFXParams.h"
#include <AK/DSP/AkDelayLineMemoryStream.h>

#include "Grain.h"
#include "LinearValueRamp.h"

#define NUMGRAINS      60
#define ENVELOPELENGTH 1024

static const AkReal32 ONEOVERMAXRANDVAL = (1.f / 0x80000000); // 2^31
static const AkReal64 DOUBLE_PI = 3.1415926535897;

class GranulatorFXDSP
{
public:
    GranulatorFXDSP();
    ~GranulatorFXDSP();

    void Setup(GranulatorFXParams* pInitialParams, bool isSendMode, AkUInt32 sampleRate);
    AKRESULT InitDelay(AK::IAkPluginMemAlloc* pAllocator, GranulatorFXParams* pParams, AkChannelConfig channelConfig);
    void ResetDelay();
    void TermDelay(AK::IAkPluginMemAlloc* pAllocator);
    void ComputeTailLength(bool feedbackEnabled, AkReal32 feedbackValue);

    //==============================================================================
    void Process(AkAudioBuffer* io_pBuffer, GranulatorFXParams* pCurrentParams);

private:

    void GenerateEnvelope(AkReal32* pEnvelopeTable);
    void ActivateGrain();

    //==============================================================================
    AkReal32 PseudoRandomNumber();
    AkReal32 SemitonesToPitchRatio(AkReal32 semitones);

    //==============================================================================
#ifdef AK_VOICE_MAX_NUM_CHANNELS
    AK::DSP::CAkDelayLineMemoryStream<AkReal32, AK_VOICE_MAX_NUM_CHANNELS> m_delayMemory;
#else
    AK::DSP::CAkDelayLineMemory<AkReal32> m_delayMemory;
#endif

    Grain                   m_grains[NUMGRAINS];
    LinearValueRamp         m_inputGainRamp, m_blendRamp;
    AkFXTailHandler         m_AkFXTailHandler;
    GranulatorFXParams      m_params;

    AkReal32* AK_RESTRICT   m_pDelayData;
    AkReal32                m_envelope[ENVELOPELENGTH];
    AkInt32                 m_interonsetSamples = 1024; // Arbitrary starting interonet time
    AkUInt32                m_delayPosition;
    AkUInt32                m_numProcessedChannels;
    AkUInt32                m_sampleRate;
    AkUInt32                m_tailLength;

    bool                    m_sendMode;
    bool                    m_processLFE;

    static AkUInt32         s_randomSeed;
    static AkReal32         s_twelfthRootOfTwo;

} AK_ALIGN_DMA;

#endif // GRANULATORFXDSP_H_