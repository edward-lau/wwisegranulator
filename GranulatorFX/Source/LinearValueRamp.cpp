#include "LinearValueRamp.h"
#include <AK/Tools/Common/AkAssert.h>
#include <math.h>

LinearValueRamp::LinearValueRamp()
{
    
}

LinearValueRamp::~LinearValueRamp()
{
    
}

void LinearValueRamp::Setup(AkUInt32 sampleRate, AkReal32 rampTime)
{
    AKASSERT(sampleRate > 0 && rampTime >= 0); // assertion failure if sample rate and smooth time is 0
    
    stepsToTarget = (AkUInt32)floor(rampTime * sampleRate);
    currentValue = targetValue;
    countDown = 0;
}

void LinearValueRamp::SetTarget(AkReal32 newValue)
{
    if (targetValue != newValue) // if we have a new target...
    {
        targetValue = newValue; // set new target
        countDown = stepsToTarget; // reset number of steps to count down to target
        
        if (countDown <= 0)
        {
            currentValue = targetValue;
        }
        else
        {
            step = (targetValue - currentValue) / (AkReal32)countDown; // get increment per step
        }
    }
}

float LinearValueRamp::GetNextValue()
{
    if (countDown <= 0) // if we have finished counting down to target, return target
    {
        return targetValue;
    }
    
    --countDown;
    currentValue += step; // increment current value
    return currentValue;
}
