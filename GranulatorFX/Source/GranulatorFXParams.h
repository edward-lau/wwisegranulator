#ifndef GRANULATORFXPARAMS_H_
#define GRANULATORFXPARAMS_H_

#include <AK/SoundEngine/Common/IAkPlugin.h>

// Parameters IDs for the Wwise or RTPC.
// Those IDs map to the AudioEnginePropertyID attributes in the XML property definition
static const AkPluginParamID AK_PARAM_ID_INPUTGAIN      = 0;
static const AkPluginParamID AK_PARAM_ID_BLEND          = 1;
static const AkPluginParamID AK_PARAM_ID_RANDOMLEVEL    = 2;
static const AkPluginParamID AK_PARAM_ID_RANDOMPAN      = 3;
static const AkPluginParamID AK_PARAM_ID_DELAY          = 4;
static const AkPluginParamID AK_PARAM_ID_RANDOMDELAY    = 5;
static const AkPluginParamID AK_PARAM_ID_PITCH          = 6;
static const AkPluginParamID AK_PARAM_ID_RANDOMPITCH    = 7;
static const AkPluginParamID AK_PARAM_ID_DURATION       = 8;
static const AkPluginParamID AK_PARAM_ID_RANDOMDURATION = 9;
static const AkPluginParamID AK_PARAM_ID_DENSITY        = 10;

static const AkPluginParamID AK_PARAM_ID_PROCESSLFE = 11;

// Delay line buffer size
#define MAXDELAYTIME                    (2.0f)      // (seconds)

// Default parameters

//RTPC
#define PARAM_DEFAULT_INPUTGAIN         (0.5f)     // 0.0 - 1.0
#define PARAM_DEFAULT_BLEND             (0.5f)     // 0.0 - 1.0
#define PARAM_DEFAULT_RANDOMLEVEL       (-6.0f)    // -40.0 - 0.0 dbFs
#define PARAM_DEFAULT_RANDOMPAN         (0.0f)     // -1.0 - 1.0
#define PARAM_DEFAULT_DELAY             (0.5f)     // 0.0 - 1.0 (relative to delayline size)
#define PARAM_DEFAULT_RANDOMDELAY       (0.5f)     // 0.01- 1.0 (relative to delayline size)
#define PARAM_DEFAULT_PITCH             (0.0f)     // -12.0 - 12.0 (semitones)
#define PARAM_DEFAULT_RANDOMPITCH       (0.0f)     // 0.0 - 12.0 (semitones)
#define PARAM_DEFAULT_DURATION          (100.0f)   // 16.0 - 900.0 (milliseconds)
#define PARAM_DEFAULT_RANDOMDURATION    (0.0f)     // 0.0 - 870.0 (milliseconds)
#define PARAM_DEFAULT_DENSITY           (100.0f)   // 1.1 - 500 (grain density per unit time)

//Non-RTPC
#define PARAM_DEFAULT_PROCESSLFE        (true)

struct GranulatorRTPCParams
{
    AkReal32 inputGain;
    AkReal32 blend;
    AkReal32 randomLevel;
    AkReal32 randomPan;
    AkReal32 delay;
    AkReal32 randomDelay;
    AkReal32 pitch;
    AkReal32 randomPitch;
    AkReal32 duration;
    AkReal32 randomDuration;
    AkReal32 density;
    bool hasChanged;
};

struct GranulatorNonRTPCParams
{
//     AkReal32 maxDelayTime;
    bool processLFE;
    bool hasChanged;
};

struct GranulatorParams
{
    GranulatorRTPCParams RTPC;
    GranulatorNonRTPCParams NonRTPC;
} AK_ALIGN_DMA;

class GranulatorFXParams : public AK::IAkPluginParam,
                           public GranulatorParams
{
public:
    GranulatorFXParams();
    GranulatorFXParams(const GranulatorFXParams& copy);
    ~GranulatorFXParams();

    IAkPluginParam* Clone(AK::IAkPluginMemAlloc* in_pAllocator);

    AKRESULT Init(AK::IAkPluginMemAlloc* in_pAllocator, 
                  const void*            in_pParamsBlock, 
                  AkUInt32               in_uBlockSize);

    AKRESULT Term(AK::IAkPluginMemAlloc* in_pAllocator);
    
    AKRESULT SetParamsBlock(const void* in_pParamsBlock, 
                            AkUInt32    in_uBlockSize);

    AKRESULT SetParam(AkPluginParamID in_paramID, 
                      const void*     in_pValue, 
                      AkUInt32        in_uParamSize);
};

#endif // GRANULATORFXPARAMS_H_
