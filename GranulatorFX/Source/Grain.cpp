#include "Grain.h"
#include <math.h>

#include <AK/Tools/Common/AkPlatformFuncs.h>

Grain::Grain()
{
}

Grain::~Grain()
{
}

// Initialize grain before use!
void Grain::Initialize(const AkReal32 maxDelayTime,
                       AkReal32*      pEnvelopeTable,
                       const AkUInt32 envSize,
                       const AkUInt32 sampleRate)
{
    m_maxDelayTime = maxDelayTime;
    m_pEnvelope    = pEnvelopeTable;
    m_envelopeSize = envSize;
    m_sampleRate   = sampleRate;
}

// Prepare grain before activating
void Grain::Activate(GrainParameters newParams)
{
    m_params = newParams;
    
    // Reset envelope read position and calculate new increment based on new duration
    m_envReadPos        = 0;
    m_envIncrement      = (AkReal32)m_envelopeSize / m_params.durationSamples;

    isActive = true;
}

// Process an array containing left and right samples
void Grain::Process(AkReal32& outputSample, AkReal32* AK_RESTRICT pDelayData)
{
    AkReal32 delayTimeSamples = m_params.delayTime * m_sampleRate;
    const AkUInt32 maxDelayTimeSamples = (AkUInt32)(m_maxDelayTime * m_sampleRate);
    m_pDelayBuffer = pDelayData;

    if (delayTimeSamples > maxDelayTimeSamples)
    {
        delayTimeSamples = (AkReal32)maxDelayTimeSamples;
    }

    // Get reading position
    AkReal32 readPos = m_params.delayPosition - delayTimeSamples;

    // Check if reading position is within buffer range, modulo if necessary
    readPos = (readPos >= 0 ? (readPos < maxDelayTimeSamples ? readPos : readPos - maxDelayTimeSamples) : readPos + maxDelayTimeSamples);

    // Delay buffer linear interpolation
    const AkUInt32 readIndex = (AkUInt32)readPos; // get index to the left of the reading position
    const AkReal32 frac = readPos - readIndex; // get the "fraction", distance of reading position to the index
    const AkUInt32 nextIndex = (readIndex != maxDelayTimeSamples - 1 ? readIndex + 1 : 0); // get the next index, wraparound if necessary

    // Envelope table linear interpolation
    const AkUInt32 envIndex = (AkUInt32)m_envReadPos;
    const AkReal32 envFrac = m_envReadPos - envIndex;
    const AkUInt32 envNextIndex = envIndex + 1;

    // Interpolate envelope table value
    const AkReal32 envelopeOut = (m_pEnvelope[envIndex] + envFrac * (m_pEnvelope[envNextIndex] - m_pEnvelope[envIndex]));

    // Interpolate delay buffer value and multiply with envelope
    const AkReal32 output = (m_pDelayBuffer[readIndex] + frac * (m_pDelayBuffer[nextIndex] - m_pDelayBuffer[readIndex])) * envelopeOut;

    // Add grain to output sample
    outputSample += output * m_params.level;

    // Increment envelope reading positions
    m_envReadPos += m_envIncrement;

    // Increment reading position, modulo if necessary
    m_params.delayPosition += m_params.pitchRatio;
    m_params.delayPosition = (m_params.delayPosition < maxDelayTimeSamples ? m_params.delayPosition : m_params.delayPosition - maxDelayTimeSamples);

    // Count down grain duration and deactivate if necessary
    --m_params.durationSamples;
    if (m_params.durationSamples <= 0)
    {
        isActive = false;
    }
}
