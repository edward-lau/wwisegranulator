#ifndef GRAIN_H_
#define GRAIN_H_

#include <AK/SoundEngine/Common/AkTypes.h>

struct GrainParameters
{
    AkReal32 delayPosition;
    AkReal32 delayTime;
    AkReal32 level;
    AkReal32 pitchRatio;
    AkInt32 durationSamples;
};

class Grain
{
public:
    Grain();
    ~Grain();

    bool isActive = false;

    void Initialize(const AkReal32 maxDelayTime,
                    AkReal32*      pEnvelopeTable,
                    const AkUInt32 envelopeSize,
                    const AkUInt32 sampleRate);

    void Activate(GrainParameters newParams);
    void Process(AkReal32& outputSample, AkReal32* AK_RESTRICT pDelayData);

private:
    GrainParameters m_params;

    AkReal32* m_pDelayBuffer; // pointer to granulator delay buffer
    AkReal32* m_pEnvelope; // pointer to envelope buffer
    
    AkReal32 m_maxDelayTime;
    AkReal32 m_envReadPos, m_envIncrement;
    AkUInt32 m_envelopeSize;
    AkUInt32 m_sampleRate;
};

#endif // GRAIN_H_