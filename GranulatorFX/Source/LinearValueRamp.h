#ifndef LINEARVALUERAMP_H_
#define LINEARVALUERAMP_H_

#include <AK/SoundEngine/Common/AkTypes.h>

class LinearValueRamp
{
public:
    LinearValueRamp();
    ~LinearValueRamp();
    
    void Setup (AkUInt32 sampleRate, AkReal32 rampTime);
    void SetTarget (AkReal32 newValue);
    AkReal32 GetNextValue ();

private:
    AkReal32 currentValue = 0, targetValue = 0, step = 0;
    AkInt32 countDown = 0;
    AkUInt32 stepsToTarget = 0;
};

#endif // SMOOTHVALUE_H_
