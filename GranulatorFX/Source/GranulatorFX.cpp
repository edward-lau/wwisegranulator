#include "GranulatorFX.h"
#include <AK/Tools/Common/AkAssert.h>
#include <AK/AkWwiseSDKVersion.h>

AK::IAkPlugin* CreateGranulatorFX(AK::IAkPluginMemAlloc* pAllocator)
{
    return AK_PLUGIN_NEW(pAllocator, GranulatorFX());
}

AK::IAkPluginParam* CreateGranulatorFXParams(AK::IAkPluginMemAlloc* pAllocator)
{
    return AK_PLUGIN_NEW(pAllocator, GranulatorFXParams());
}

AK_IMPLEMENT_PLUGIN_FACTORY(GranulatorFX, AkPluginTypeEffect, 100, 101)

GranulatorFX::GranulatorFX()
: m_pParams(NULL),
  m_pAllocator(NULL)
{
}

GranulatorFX::~GranulatorFX()
{
}

/*
-Initializes plug-in and allocates memory.
-Called everytime the plug-in is instantiated (ie. when a voice starts playing, mixing bus instantiated).
*/
AKRESULT GranulatorFX::Init(AK::IAkPluginMemAlloc*      pAllocator,         // memory allocator interface
                            AK::IAkEffectPluginContext* pEffectContext,     // sound-engine execution context
                            AK::IAkPluginParam*         pParams,            // effect parameter node
                            AkAudioFormat&              format)             // input/output audio format
{
    m_pParams = (GranulatorFXParams*)pParams;
    m_pAllocator = pAllocator;

    m_FXState.Setup(m_pParams, pEffectContext->IsSendModeEffect(), format.uSampleRate);
    AKRESULT initResult = m_FXState.InitDelay(pAllocator, m_pParams, format.channelConfig);
    // compute tail length here?
    m_pParams->NonRTPC.hasChanged = false;
    m_pParams->RTPC.hasChanged = false;
    
    AK_PERF_RECORDING_RESET();

    return initResult;
}

/*
-Called when plug-in is terminated.
-Release all memory resources used by plug-in and self-destruct.
*/
AKRESULT GranulatorFX::Term(AK::IAkPluginMemAlloc* pAllocator)
{
    m_FXState.TermDelay(pAllocator);
    AK_PLUGIN_DELETE(pAllocator, this);

    return AK_Success;
}

/*
-Prepares plug-in to accomodate new unrelated audio content.
-Called after initialization and any other time the state of the object needs to be reset (ie. prepareToPlay in JUCE?)
*/
AKRESULT GranulatorFX::Reset()
{
    // Reset DSP
    m_FXState.ResetDelay();
    return AK_Success;
}

AKRESULT GranulatorFX::GetPluginInfo(AkPluginInfo& pluginInfo)
{
    pluginInfo.eType = AkPluginTypeEffect;
    pluginInfo.bIsInPlace = true;
    pluginInfo.uBuildVersion = AK_WWISESDK_VERSION_COMBINED;

    return AK_Success;
}

/*
-Execute plug-in signal processing.
*/
void GranulatorFX::Execute(AkAudioBuffer* io_pBuffer)
{
    if (AK_EXPECT_FALSE(m_pParams->NonRTPC.hasChanged))
    {
        AKRESULT initResult = m_FXState.InitDelay(m_pAllocator, m_pParams, io_pBuffer->GetChannelConfig());
        if (initResult != AK_Success)
            return;

        m_FXState.ResetDelay();
        m_pParams->NonRTPC.hasChanged = false;
    }

    if (AK_EXPECT_FALSE(m_pParams->RTPC.hasChanged))
    {
        // compute tail length here?
        m_pParams->RTPC.hasChanged = false;
    }

    m_FXState.Process(io_pBuffer, m_pParams);
}