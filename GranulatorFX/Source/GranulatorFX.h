#ifndef GRANULATORFX_H_
#define GRANULATORFX_H_

#include "GranulatorFXParams.h"
#include "GranulatorFXDSP.h"

class GranulatorFX : public AK::IAkInPlaceEffectPlugin
{
public:
    GranulatorFX();
    ~GranulatorFX();

    AKRESULT Init(AK::IAkPluginMemAlloc*      pAllocator, 
                  AK::IAkEffectPluginContext* pEffectContext,
                  AK::IAkPluginParam*         pParams, 
                  AkAudioFormat&              format);
    
    AKRESULT Term(AK::IAkPluginMemAlloc* pAllocator);
    AKRESULT Reset();
    AKRESULT GetPluginInfo(AkPluginInfo& pluginInfo);

    void Execute(AkAudioBuffer* io_pBuffer);

    AKRESULT TimeSkip(AkUInt32 in_uFrames) { return AK_DataReady; }

private:
    GranulatorFXDSP             m_FXState;
    GranulatorFXParams*         m_pParams;
    AK::IAkPluginMemAlloc*      m_pAllocator;
};

#endif // GRANULATORFX_H_